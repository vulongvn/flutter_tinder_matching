final List<Profile> demoProfiles = [
  new Profile(
    photos: [
      'assets/photo_1.jpg',
      'assets/photo_2.jpg',
      'assets/photo_3.jpg',
      'assets/photo_4.jpg',
    ],
    name: 'Someone Special',
    bio: 'This is the person you want',
  ),
  new Profile(
    photos: [
      'assets/photo_1.jpg',
      'assets/photo_2.jpg',
      'assets/photo_3.jpg',
      'assets/photo_4.jpg',
    ],
    name: 'Vu Long',
    bio: 'So handsome',
  ),
  new Profile(
    photos: [
      'assets/photo_1.jpg',
      'assets/photo_2.jpg',
      'assets/photo_3.jpg',
      'assets/photo_4.jpg',
    ],
    name: 'Vu Long 2',
    bio: 'So handsome',
  ),
  new Profile(
    photos: [
      'assets/photo_1.jpg',
      'assets/photo_2.jpg',
      'assets/photo_3.jpg',
      'assets/photo_4.jpg',
    ],
    name: 'Vu Long 3',
    bio: 'So handsome',
  ),
];

class Profile {
  final List<String> photos;
  final String name;
  final String bio;

  Profile({
    this.photos,
    this.name,
    this.bio,
  });
}